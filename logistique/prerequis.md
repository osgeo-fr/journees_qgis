# Texte à mettre en ligne sur le site Osgeo.fr

- [ ] à mettre sur la page d'accueil
- [ ] créer une page ou un nouvel onglet

## Prochaine édition 2024 - pré-requis organisation

Osgeo-fr travaille déjà sur la prochaine **édition 2024** des **Rencontres QGIS Utilisateurs** !

Si vous êtes intéressés pour accueillir l'événement faites vous connaître qgis-conf AT osgeo.asso.fr

Voici quelques pré-requis pour accueillir les deux jours des Rencontres :

- le premier jour est consacré aux ateliers. Il s'agit d'ateliers par groupe de 15 à 20 participants. Habituellement on met 5 à 6 ateliers en place, il faut donc prévoir 5 à 6 salles.

- le deuxième jour, ce sont les conférences en plenière avec une salle adaptée. Il faut pouvoir projeter et faire un enregistrement vidéo de la journée (slides et conférencier) pour la diffusion post-événement.

- pour assurer le lien, les échanges et la convivialité, un endroit où les participants puissent se restaurer et faire des pauses (salle, réfectoire, cantine).

**Merci pour l'intérêt et votre soutien au projet QGIS**


