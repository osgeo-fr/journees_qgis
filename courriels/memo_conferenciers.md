Bonjour,

Le 14 mars prochain, vous allez assurer une conférence dans le cadre des journées utilisateurs QGIS France organisées par l'OSGeo-fr en partenariat avec le CRAIG à l'Hôtel de Région de Clermont-Ferrand. Nous vous remercions de votre contribution.
Vous trouverez ci-après quelques informations relatives à la préparation et au déroulement des journées:

- Vous pouvez retrouver l’heure de votre présentation sur la page programme[0] de notre site internet.
- En tant que présentateur ou présentatrice, il va sans dire que vous êtes convié.e à cette journée. Toutefois, et afin de faciliter l'organisation des journées, nous vous remercions de bien vouloir vous inscrire via la page HelloAsso [1] à l'aide de ce code promo: XXXX. Pensez également à indiquer si vous envisagez de participer au pot du lundi soir et au repas du mardi midi.
- En cas de souci ou de question de dernière minute, vous pouvez nous joindre au 0800xxxxxx ou au 0800yyyyyy. Pourriez-vous également nous donner votre numéro de téléphone pour pouvoir vous joindre, le cas échéant ?
- La captation et l'animation technique de ces journées relevant des services techniques de l'Hôtel de Région, nous devons nous conformer à quelques exigences et vous remercions de bien vouloir nous:

  - fournir les civilité, nom, prénom, fonction et structure de chaque intervenant
  - transmettre vos présentations au format PDF (xxx@yyy.fr) au plus tard le **25 février 2023**.
  - nous vous rappelons que toutes les interventions seront filmées. Leur retransmission en direct ne sera finalement pas possible, mais les vidéos seront mises en ligne sur notre chaîne Youtube par la suite.

Nous restons à votre disposition pour toute question ou complément d’information nécessaire à la bonne organisation de cette journée de conférences.

Cordialement,
L’équipe d’organisation

[0] https://conf.qgis.osgeo.fr/z20_programme.html

[1] https://www.helloasso.com/associations/osgeo-fr/evenements/journee-utilisateur-qgis-francophone-2023-2
