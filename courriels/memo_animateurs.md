Bonjour,

Le 13 mars prochain, vous allez animer un atelier dans le cadre des journées utilisateurs QGIS France organisées par l'OSGeo-fr en partenariat avec le CRAIG à l'Hôtel de Région de Clermont-Ferrand. Nous vous remercions de votre contribution.
Vous trouverez ci-après quelques informations relatives à la préparation et au déroulement des journées:

- Vous pouvez retrouver l’heure et la salle dédiée à votre(vos) atelier(s) sur la page programme [0] de notre site internet. Un dispositif de vidéoprojection est disponible dans chaque salle : vous pouvez si vous le souhaitez venir avec un ordinateur portable. Un membre de l’organisation sera présent avec vous pour le lancement de l’atelier.
- L’équipe organisatrice sera présente toute la journée dès 8h30 : si vous le souhaitez, vous pouvez venir un peu plus tôt pour organiser la salle comme vous le souhaitez. En cas de souci ou de question de dernière minute, vous pouvez également nous joindre au xxxx
- En tant qu'animateur.rice, vous êtes convié.e. à la journée de conférences; il vous suffit de vous inscrire sur le site d'inscription HelloAsso [1] à l'aide du code promo ci-après: XXXX, en précisant si vous souhaitez participer au pot du lundi soir et manger le mardi midi. Bien évidemment, pas besoin de s'inscrire pour l’atelier que vous animez.
Toutefois, pour faciliter notre organisation, pouvez-vous nous préciser par retour de mail si vous serez présent(e) au repas du lundi midi ? Pourriez-vous également nous donner votre numéro de téléphone pour pouvoir vous joindre en cas de besoin de dernière minute ?
- Merci de nous transmettre les éventuels pré-requis (version recommandée de QGIS, plugins, détails de connexion,....) dont les stagiaires de votre atelier pourraient avoir besoin. Nous nous chargeons de les publier sur le site du programme (et si des informations "confidentielles" sont concernées, nous les leur transmettrons directement)

Nous restons à votre disposition pour toute question ou complément d’information nécessaire à la bonne organisation de cette journée d’ateliers.
Cordialement,
L’équipe d’organisation

[0] https://conf.qgis.osgeo.fr/z20_programme.html

[1] https://www.helloasso.com/associations/osgeo-fr/evenements/journee-utilisateur-qgis-francophone-2023-2
